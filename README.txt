INTRODUCTION
------------

Sirdata CMP is a Consent Management Platform made to reply to privacy regulations such like GDPR, ePrivacy, CCPA and already used by thousands of websites.
Sirdata provides a CMP certified (IAB Transparency & Consent Framework V2.1) and compatible with all players in the programmatic and eCommerce markets.

* For a full description of the module, visit the project page: https://www.drupal.org/project/cmp_sirdata
* To submit bug reports and feature suggestions, or track changes: https://www.drupal.org/project/issues/cmp_sirdata

REQUIREMENTS
------------

This module requires no modules outside of Drupal core, but you'll need to create an account in CMP Sirdata: https://cmp.sirdata.io/

INSTALLATION
------------

1. Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.
2. Enable the module.

CONFIGURATION
-------------

1. Navigate to http://yoursite.com/admin/config/system/cmp-sirdata
2. Enter your app_key and customer_key and click save.
3. Configure the user permissions in Administration » People » Permissions

MAINTAINERS
-----------

Current maintainers:
 * Skilld - https://www.drupal.org/skilld

This project has been sponsored by:
 * Sirdata - https://sirdata.com/
