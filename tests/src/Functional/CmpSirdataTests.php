<?php

namespace Drupal\Tests\cmp_sirdata\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test CMP Sirdata.
 *
 * @group cmp_sirdata
 */
class CmpSirdataTests extends BrowserTestBase {

  /**
   * A simple user.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * The theme to install as the default for testing.
   *
   * @var string
   */
  protected $defaultTheme = 'claro';

  /**
   * Modules to enable.
   *
   * @var array
   */
  static protected $modules = ['cmp_sirdata'];

  /**
   * Perform initial setup tasks that run before every test method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'administer cmp sirdata configuration',
    ]);
  }

  /**
   * Test if the API keys was saved properly.
   *
   * @param bool $enable
   *   Enable scripts for the pages.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function saveApiKeys($enable = TRUE) {
    // Login.
    $this->drupalLogin($this->user);

    // Save the form.
    $this->drupalGet("admin/config/system/cmp-sirdata");
    $data = [];
    $data['enable'] = $enable;
    $data['customer_key'] = '29609';
    $data['app_key'] = '8LLpc';
    $this->submitForm($data, 'Save configuration');
    $this->assertSession()
      ->pageTextContains('The configuration options have been saved');

    // Get keys.
    $settings = \Drupal::config('cmp_sirdata.settings');
    $this->assertSame($settings->get('customer_key'), '29609', 'Correct customer_key configuration saved found.');
    $this->assertSame($settings->get('app_key'), '8LLpc', 'Correct app_key configuration saved found.');

    // Enabled.
    if ($enable) {
      $this->assertSame($settings->get('enable'), TRUE);
    }
    // Disabled.
    else {
      $this->assertSame($settings->get('enable'), FALSE);
    }
  }

  /**
   * Test if the scripts is at html for anonymous.
   */
  public function testScriptsInAnonymousPages() {
    $this->saveApiKeys(TRUE);
    // Go to reset password page.
    $this->drupalLogout();
    $this->drupalGet('user/password');
    $this->assertSession()->statusCodeEquals(200);

    // Check if the scripts exist.
    $page = $this->getSession()->getPage();
    $this->assertStringContainsString(
      '<script src="https://cache.consentframework.com/js/pa/8LLpc/c/29609/stub" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page contains the cache correct script'
    );
    $this->assertStringContainsString(
      '<script src="https://choices.consentframework.com/js/pa/8LLpc/c/29609/cmp" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page contains the choices correct script'
    );

    $this->saveApiKeys(FALSE);
    // Go to reset password page.
    $this->drupalLogout();
    $this->drupalGet('user/password');
    $this->assertSession()->statusCodeEquals(200);

    // Check if the scripts don't exist.
    $page = $this->getSession()->getPage();
    $this->assertStringNotContainsString(
      '<script src="https://cache.consentframework.com/js/pa/8LLpc/c/29609/stub" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page does not contain the cache correct script'
    );
    $this->assertStringNotContainsString(
      '<script src="https://choices.consentframework.com/js/pa/8LLpc/c/29609/cmp" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page does not contain the choices correct script'
    );
  }

  /**
   * Test if the scripts is at html for authenticated.
   */
  public function testScriptsInAuthPages() {
    $this->saveApiKeys(TRUE);
    // Go to reset password page.
    $this->drupalGet('user/password');
    $this->assertSession()->statusCodeEquals(200);

    // Check if the scripts exist.
    $page = $this->getSession()->getPage();
    $this->assertStringContainsString(
      '<script src="https://cache.consentframework.com/js/pa/8LLpc/c/29609/stub" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page contains the cache correct script'
    );
    $this->assertStringContainsString(
      '<script src="https://choices.consentframework.com/js/pa/8LLpc/c/29609/cmp" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page contains the choices correct script'
    );

    $this->saveApiKeys(FALSE);
    // Go to reset password page.
    $this->drupalGet('user/password');
    $this->assertSession()->statusCodeEquals(200);

    // Check if the scripts don't exist.
    $page = $this->getSession()->getPage();
    $this->assertStringNotContainsString(
      '<script src="https://cache.consentframework.com/js/pa/8LLpc/c/29609/stub" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page does not contain the cache correct script'
    );
    $this->assertStringNotContainsString(
      '<script src="https://choices.consentframework.com/js/pa/8LLpc/c/29609/cmp" referrerpolicy="unsafe-url" charset="utf-8" type="text/javascript"></script>',
      $page->getHtml(),
      'The page does not contain the choices correct script'
    );
  }

  /**
   * Test if the scripts is at html for authenticated with big pipe enabled.
   * @doesNotPerformAssertions
   */
  public function testScriptsInAuthAndBigPipePages() {
    \Drupal::service('module_installer')->install(['big_pipe']);
    $this->testScriptsInAuthPages();
  }

}
