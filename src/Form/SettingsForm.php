<?php

namespace Drupal\cmp_sirdata\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure CMP Sirdata settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cmp_sirdata_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cmp_sirdata.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cmp_sirdata.settings');
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CMP Sirdata'),
      '#default_value' => $config->get('enable'),
      '#description' => $this->t('Enable or disable this feature.'),
    ];
    $form['customer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CMP Sirdata Customer Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('customer_key'),
      '#description' => $this->t('The Sirdata customer key.'),
    ];
    $form['app_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CMP Sirdata App Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('app_key'),
      '#description' => $this->t('The Sirdata application key.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('cmp_sirdata.settings')
      ->set('enable', $form_state->getValue('enable'))
      ->set('customer_key', trim($form_state->getValue('customer_key')))
      ->set('app_key', trim($form_state->getValue('app_key')))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
